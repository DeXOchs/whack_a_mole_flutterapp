# whack_a_mole_flutterapp

Flutter App to play whack a mole with eSense BLE device controls.
The game only works with the eSense BLE devices (https://www.esense.io/), because you must control a game entity with these controls. Therefor the device to which the app is deployed must be BLE enabled.
