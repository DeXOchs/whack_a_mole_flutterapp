import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Hammer extends StatefulWidget {
  Hammer({Key key, this.positionX, this.positionY, this.hitCounter}) : super(key: key);
  
  final double positionX;
  final double positionY;
  final int hitCounter;
  
  @override
  State<StatefulWidget> createState() => _HammerState();
}

class _HammerState extends State<Hammer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.height * 0.08,
        height: MediaQuery.of(context).size.height * 0.08,
        alignment: Alignment.center,
        child: Image(
          image: AssetImage('assets/hammer.png'),
          fit: BoxFit.cover,
        ),
      ),
      alignment: Alignment(widget.positionX, widget.positionY),
    );
  }
}