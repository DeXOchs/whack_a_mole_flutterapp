import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:esense_flutter/esense.dart';
import 'package:flutter_esense_app/WamGame.dart';
import 'package:flutter_esense_app/StatefulMotionHandler.dart';

class GameEnvironment extends StatefulWidget {
  @override
  _GameEnvironmentState createState() => new _GameEnvironmentState();
}

class _GameEnvironmentState extends State<GameEnvironment> {
  // environment state
  bool isGameRunning;
  int bestHitScore;

  // controls
  double hammerPositionX;
  double hammerPositionY;
  
  // timing
  Timer timer;
  
  // eSense properties
  final String eSenseName = 'eSense-0091';
  final int fixedAlphaGyroOffset = 85;
  final int fixedGammaGyroOffset = 37;
  final StatefulMotionHandler alphaMotionHandler = new StatefulMotionHandler(buffersize: 1);
  final StatefulMotionHandler gammaMotionHandler = new StatefulMotionHandler(buffersize: 1);
  String eSenseConnectionStatus = '';
  double eSenseBatteryLoad = -1;
  bool sampling = false;
  StreamSubscription eSenseSensorEventSubscription;
  
  @override
  void initState() {
    super.initState();
    isGameRunning = false;
    bestHitScore = 0;
    hammerPositionX = 0;
    hammerPositionY = 0;
    connectToESense();
  }
  
  void dispose() {
    cancelListenToSensorEvents();
    ESenseManager.disconnect();
    super.dispose();
  }
  
  Widget build(BuildContext context) {
    if (this.isGameRunning) {
      return new WamGame(
        onGameAbortCallback: shutdownGame,
        onGameFinishCallback: onGameFinish,
        hammerPositionX: this.hammerPositionX,
        hammerPositionY: this.hammerPositionY,
      );
    } else {
      return CupertinoPageScaffold(
        backgroundColor: Colors.lightGreenAccent,
        child: Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 20),
              Text(
                'Set up Game',
                style: TextStyle(fontSize: 25),
              ),
              SizedBox(height: 30),
              GridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 0,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text('Status', textScaleFactor: 1.5),
                      Text(this.eSenseConnectionStatus),
                      !ESenseManager.connected ? CupertinoActivityIndicator() : Icon(Icons.bluetooth_connected, color: CupertinoColors.black,),
                    ],
                  ),
                  if (ESenseManager.connected && this.eSenseBatteryLoad != -1) Column(
                    children: <Widget>[
                      Text('Battery', textScaleFactor: 1.5),
                      Text((this.eSenseBatteryLoad / 0.042).round().toString() + '%'),
                      Icon(CupertinoIcons.battery_full, color: CupertinoColors.black)
                    ],
                  )
                ],
              ),
              if (!ESenseManager.connected) Padding(
                padding: EdgeInsets.only(left: 60, right: 60, bottom: 100),
                child: Text(
                  'Wait patiently for the BLE connection to be established to the eSense device.',
                  textAlign: TextAlign.center
                ),
              ),
              if (ESenseManager.connected) Padding(
                padding: EdgeInsets.only(left: 70, right: 70, bottom: 100),
                child: Text(
                  'Hold this device and \nyour head in position and \npress "Start Game".',
                  textAlign: TextAlign.center
                ),
              ),
              if (this.bestHitScore != 0) Padding(
                padding: EdgeInsets.only(top: 00, bottom: 20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Best so far: $bestHitScore Hits',
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(width: 5,),
                    ImageIcon(
                      AssetImage("assets/hammer_icon.png"),
                      color: CupertinoColors.black,
                    ),
                  ],
                ),
                /*
                child: Text(
                  'Best so far: $bestHitScore Hits!',
                  textAlign: TextAlign.center,
                ),
                 */
              ),
              
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: CupertinoButton(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(CupertinoIcons.play_arrow_solid, color: CupertinoColors.white),
                      SizedBox(width: 10),
                      Text('Start Game')
                    ],
                  ),
                  onPressed: (!ESenseManager.connected) ? null : startGame,
                  color: CupertinoColors.systemBlue
                ),
              ),
            ],
          ),
        )
      );
    }
  }

  Future<void> connectToESense() async {
    bool isConnected = false;
  
    // set up listener before connecting
    ESenseManager.connectionEvents.listen((event) {
      print('CONNECTION event: $event');
    
      // when we're connected to the eSense device, we can start listening to events from it
      if (event.type == ConnectionType.connected) listenToESenseEvents();
    
      setState(() {
        switch (event.type) {
          case ConnectionType.connected:
            eSenseConnectionStatus = 'connected';
            break;
          case ConnectionType.unknown:
            eSenseConnectionStatus = 'unknown';
            break;
          case ConnectionType.disconnected:
            isGameRunning = false;
            sampling = false;
            eSenseConnectionStatus = 'disconnected';
            eSenseBatteryLoad = -1;
            break;
          case ConnectionType.device_found:
            eSenseConnectionStatus = 'device found';
            break;
          case ConnectionType.device_not_found:
            eSenseConnectionStatus = 'no device found';
            break;
        }
      });
    });
  
    isConnected = await ESenseManager.connect(eSenseName);
  
    setState(() {
      eSenseConnectionStatus = isConnected ? 'connecting ...' : 'connection failed';
    });
  }

  void listenToESenseEvents() async {
    ESenseManager.eSenseEvents.listen((event) {
      print('ESENSE event: $event');
    
      if (event.runtimeType == BatteryRead) {
        setState(() {
          eSenseBatteryLoad = (event as BatteryRead).voltage;
        });
      }
    });
  
    await ESenseManager.getBatteryVoltage();
    Timer.periodic(Duration(seconds: 60), (timer) async => await ESenseManager.getBatteryVoltage());
  }

  void startListenToSensorEvents() async {
    if (!this.sampling || this.eSenseSensorEventSubscription == null) {
      print('Initialize sensorEventSubscription.');
      eSenseSensorEventSubscription = ESenseManager.sensorEvents.listen((event) {
        var hammerPositionXOffset = (alphaMotionHandler.getMovingAverage(event.gyro[0] + this.fixedAlphaGyroOffset) / 10000) * 1.4;
        var hammerPositionYOffset = (gammaMotionHandler.getMovingAverage(event.gyro[2] + this.fixedGammaGyroOffset) / 10000) * 1.4;
      
        var computedHammerPositionX = this.hammerPositionX + hammerPositionXOffset;
        var computedHammerPositionY = this.hammerPositionY + hammerPositionYOffset;
        setState(() {
          hammerPositionX = (computedHammerPositionX.abs() > 1 ? computedHammerPositionX.sign : computedHammerPositionX);
          hammerPositionY = (computedHammerPositionY.abs() > 1 ? computedHammerPositionY.sign : computedHammerPositionY);
        });
      });
      setState(() {
        sampling = true;
      });
    }
  }

  void cancelListenToSensorEvents() async {
    print('Cancel sensorEventSubscription.');
    await this.eSenseSensorEventSubscription.cancel();
    setState(() {
      sampling = false;
    });
  }

  void startGame() {
    print('Starting game.');
    this.startListenToSensorEvents();
    setState(() {
      hammerPositionX = 0;
      hammerPositionY = 0;
      isGameRunning = true;
    });
  }
  
  void shutdownGame() {
    setState(() {
      isGameRunning = false;
    });
    this.cancelListenToSensorEvents();
  }
  
  void onGameFinish(int hammerHits) {
    print('Finishing game.');
    setState(() {
      bestHitScore = hammerHits > bestHitScore ? hammerHits : bestHitScore;
    });
    this.shutdownGame();
    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Finished'),
          content: Text('$hammerHits Moles were harmed \n Your best ?'),
          actions: [
            CupertinoDialogAction(
              child: Text('No, try again'),
              isDefaultAction: true,
              onPressed: () {
                Navigator.of(context).pop();
                this.startGame();
              },
            ),
            CupertinoDialogAction(
              child: Text('Yes, let it be'),
              onPressed: () => Navigator.of(context).pop()
            )
          ],
        );
      }
    );
  }
}