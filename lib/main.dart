import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './GameEnvironment.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      color: CupertinoColors.systemGreen,
      home: GameEnvironment()
    );
    return new GameEnvironment();
  }
}