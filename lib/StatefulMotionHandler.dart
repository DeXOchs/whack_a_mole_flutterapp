import 'package:flutter/cupertino.dart';

class StatefulMotionHandler {
  
  final int buffersize;
  List<int> buffer = new List<int>();
  
  StatefulMotionHandler({@required this.buffersize});
  
  int getMovingAverage(int currentMotionValue) {
    this.buffer.add(currentMotionValue);
    if (this.buffer.length > this.buffersize) {
      this.buffer.removeAt(0);
    }
    return (this.buffer.reduce((a, b) => (a + b)) / this.buffer.length).round();
  }
}