import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_esense_app/Hammer.dart';
import './Field.dart';

class WamGame extends StatefulWidget {
  
  // position of the hammer
  final double hammerPositionX;
  final double hammerPositionY;
  final Function onGameAbortCallback;
  final Function onGameFinishCallback;
  
  WamGame({
    Key key,
    @required this.onGameAbortCallback,
    @required this.onGameFinishCallback,
    @required this.hammerPositionX,
    @required this.hammerPositionY
  }) : super(key: key);
  
  @override
  _WamGameState createState() => _WamGameState();
}

class _WamGameState extends State<WamGame> {
  // hammer
  int hammerHits = 0;
  
  // timing
  int gameDuration = 60;
  Timer timer;
  
  // signalisation
  Color scaffoldBackground;
  
  @override
  void initState() {
    super.initState();
    this.scaffoldBackground = CupertinoColors.black;
    
    Future.delayed(Duration.zero, () => showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Ready Steady ...'),
          content: Text('Hit the mole!'),
          actions: [
            CupertinoDialogAction(
              child: Text('GO'),
              isDefaultAction: true,
              onPressed: () {
                Navigator.of(context).pop();
                this.startGame();
              },
            )
          ],
        );
      }
    ));
  }
  
  void dispose() {
    super.dispose();
    this.timer?.cancel();
  }
  
  Widget build(BuildContext context) {
    if (this.scaffoldBackground != CupertinoColors.black) {
      Timer(Duration(milliseconds: 50), () => this.scaffoldBackground = CupertinoColors.black);
    }
    
    return CupertinoPageScaffold(
      backgroundColor: this.scaffoldBackground,
      navigationBar: CupertinoNavigationBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        leading: CupertinoButton(
          padding: EdgeInsets.all(0),
          onPressed: widget.onGameAbortCallback,
          child: Text('Abort'),
        ),
        middle: Text('Time left: $gameDuration Seconds'),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 30),
        child: Stack(
          children: [
            Field(
              rawHammerPositionX: widget.hammerPositionX,
              rawHammerPositionY: widget.hammerPositionY,
              onHitCallback: this.timer != null ? onMoleHit : null,
            ),
            Hammer(
              positionX: widget.hammerPositionX,
              positionY: widget.hammerPositionY,
              hitCounter: this.hammerHits,
            ),
          ]
        )
      ),
    );
  }
  
  void startGame() {
    this.timer = new Timer.periodic(
      Duration(seconds: 1),
        (Timer timer) => setState(() {
        if (gameDuration > 0) {
          gameDuration = gameDuration - 1;
        } else {
          widget.onGameFinishCallback(this.hammerHits);
        }
      })
    );
  }
  
  void onMoleHit() {
    this.hammerHits++;
    scaffoldBackground = CupertinoColors.destructiveRed;
  }
}
