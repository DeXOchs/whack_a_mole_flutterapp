import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';

class Field extends StatefulWidget {
  final double rawHammerPositionX;
  final double rawHammerPositionY;
  final Function onHitCallback;
  
  Field({
    Key key,
    this.rawHammerPositionX,
    this.rawHammerPositionY,
    @required this.onHitCallback
  });
  
  @override
  _FieldState createState() => _FieldState();
}

class _FieldState extends State<Field> {
  
  // grid properties
  int gridColumns;
  int gridRows;
  
  // mole position
  int molePositionX;
  int molePositionY;
  
  @override
  void initState() {
    super.initState();
    this.setNewMolePosition();
  }
  
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    this.setGridDimensions();
  }
  
  Widget build(BuildContext context) {
    if (widget.onHitCallback != null && this.isMoleHit()) {
      widget.onHitCallback();
      this.setNewMolePosition();
    }
    
    return generateGrid();
  }
  
  Widget generateGrid() {
    return GridView.count(
      crossAxisCount: this.gridColumns,
      physics: NeverScrollableScrollPhysics(),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      children: List.generate(
        this.gridRows * this.gridColumns,
        (index) {
          return Container(
            child: ((() {
              if (index % this.gridColumns == this.molePositionX &&
                  (index / this.gridColumns).floor() == this.molePositionY) {
                
                return Padding(
                  padding: EdgeInsets.all(10),
                  child: Image(
                    image: AssetImage('assets/mole.png'),
                    fit: BoxFit.scaleDown,
                  )
                );
              } else {
                return null;
              }
            })()),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color.fromRGBO(0, 155, 0, 1)
            ),
            //color: Colors.lightGreenAccent,
            alignment: Alignment.center,
          );
        }
      )
    );
  }
  
  void setGridDimensions() {
    var gridCellScaling = MediaQuery.of(context).size.width >= 500 ? 1 : 2;
  
    this.gridColumns = (
      (MediaQuery.of(context).size.width * MediaQuery.of(context).devicePixelRatio * gridCellScaling) /
        (301)
    ).truncate();
  
    this.gridRows = (
      (MediaQuery.of(context).size.height) /
        (MediaQuery.of(context).size.width / this.gridColumns)
    ).truncate() - (gridCellScaling - 1);
  }
  
  bool isMoleHit() {
    // transform raw hammer position to grid position (column and row)
    var computedGridColumn = ((widget.rawHammerPositionX / 2) + 0.5) * this.gridColumns;
    var computedGridRow = ((widget.rawHammerPositionY / 2) + 0.5) * this.gridRows;
    
    if (computedGridColumn.truncate() == this.molePositionX &&
        computedGridRow.truncate() == this.molePositionY) {
  
      var distanceToColumnCenter = ((computedGridColumn.truncate() - computedGridColumn).abs() - 0.5).abs();
      var distanceToRowCenter = ((computedGridRow.truncate() - computedGridRow).abs() - 0.5).abs();

      // tolerance given in relative distance to the center of a grid field
      const TOLERANCE = 0.33;
      if (distanceToColumnCenter <= TOLERANCE && distanceToRowCenter <= TOLERANCE) {
        return true;
      }
    }
    
    return false;
  }
  
  void setNewMolePosition() {
    var random = new Random();
    this.molePositionX = random.nextInt(this.gridColumns != null ? this.gridColumns : 4);
    this.molePositionY = random.nextInt(this.gridRows != null ? this.gridRows : 4);
  }
}